﻿using System.Linq;
using System.Text;
using System.Web.Mvc;
//using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ditto.AI.LeaveBookingSystem.BLL;
using Ditto.AI.LeaveBookingSystem.DAL;
using Empiricom.Framework.Common.Enums;
using Empiricom.Framework.TestUtilities;
using NUnit.Framework;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Threading.Tasks;
using System.Transactions;



namespace Ditto.AI.LeaveBookingSystem.BLL.Tests
{
    [TestFixture]
    public class LeaveBookingRepositoryTests
    {
        private LeaveBookingRepository _leaveBookingRepository = new LeaveBookingRepository();

        protected DbUnitTestHelper dbHelper;
        protected SqlTestHelper sqlTestHelper;

        [SetUp]
        public void SetUp()
        {
            var dir = Path.GetDirectoryName(GetType().Assembly.Location);
            if (dir != null)
                System.Environment.CurrentDirectory = dir;

            dbHelper = new DbUnitTestHelper("LeaveBookingSystem_Test", "LeaveBookingSystem");
            sqlTestHelper = new SqlTestHelper("LeaveBookingSystem_Test");

            try
            {
                dbHelper.AddDataSet("UserAccessLevel");
                dbHelper.AddDataSet("RequestStatus");
                dbHelper.AddDataSet("User");
                dbHelper.AddDataSet("Team");
                dbHelper.AddDataSet("TeamUser");
                dbHelper.AddDataSet("Request");

                dbHelper.Execute(DbUnitTestOperation.Insert);
            }
            catch (SqlException sqlEx)
            {
                TearDown();
                //SetUp();
                return;
            }

            this._leaveBookingRepository = new LeaveBookingRepository();

        }

        [TearDown]
        public void TearDown()
        {
            Ditto.AI.LeaveBookingSystem.DAL.Models.LeaveBookingSystemEntities.EndContext();

            dbHelper.TearDownDataSets();

            using (TransactionScope scope = new TransactionScope())
            {
                sqlTestHelper.ExecuteSql("Delete From [dbo].[Request]");
                sqlTestHelper.ExecuteSql("Delete From [dbo].[TeamUser]");
                sqlTestHelper.ExecuteSql("Delete From [dbo].[User]");
                scope.Complete();
            }
        }

        [Test]
        public void TestMethodTest()
        {
            var x = this._leaveBookingRepository.TestMethod();
             Assert.AreEqual(x, 7);
        }

        [Test]
        public void Test_CreateUser()
        {
            var users = this._leaveBookingRepository.GetUsers();
            var previousUserCount = users.Count;
            Assert.AreEqual(7, previousUserCount);

            this._leaveBookingRepository.CreateUser("ab@ditto.ai", "A", "B", 1);

            var updatedUsers = this._leaveBookingRepository.GetUsers();
            var latestUserCount = updatedUsers.Count;

            Assert.AreEqual(previousUserCount + 1, latestUserCount);
            Assert.AreEqual(8, latestUserCount);
            Assert.IsFalse(updatedUsers[0].ID == updatedUsers[1].ID);

            var updatedTeams = this._leaveBookingRepository.GetTeams();

            Assert.AreEqual(3, updatedTeams.Count);
        }

        [Test]
        public void Test_GetUsers()
        {
            var users = this._leaveBookingRepository.GetUsers();

            Assert.AreEqual(7, users.Count);
            Assert.IsTrue(users.First().ID == 1);
        }

        [Test]
        public void Test_GetUsersByEmail()
        {
            var users = this._leaveBookingRepository.GetUsersByEmail("Bob.Johnson@Ditto.ai");
            var userCount = users.Count;

            Assert.AreEqual(1, userCount);
            Assert.IsTrue(users.First().ID == 2);
        }

        [Test]
        public void Test_CreateRequest()
        {
            //do a pull of all requests in the db and count them
            var requests = this._leaveBookingRepository.GetRequests();
            var previousRequestsCount = requests.Count;

            this._leaveBookingRepository.CreateRequest(1, new System.DateTime(2019, 1, 10), new System.DateTime(2019, 11, 4), 10, "");

            var updatedRequests = this._leaveBookingRepository.GetRequests();
            var latestRequestsCount = updatedRequests.Count;

            Assert.AreEqual(previousRequestsCount+1, latestRequestsCount);
            Assert.AreEqual(7, latestRequestsCount);
            Assert.IsFalse(requests.First().ID == requests.Last().ID);
            Assert.IsTrue(requests.First().UserID == updatedRequests.Last().UserID);
            Assert.IsTrue(updatedRequests.Last().UserID == 1);
        }

        [Test]
        public void Test_GetRequests()
        {
            var requests = this._leaveBookingRepository.GetRequests();

            Assert.AreEqual(6, requests.Count);
            Assert.IsTrue(requests.First().ID == 1);
            Assert.IsTrue(requests.Last().ID == 6);
            Assert.GreaterOrEqual(requests.Last().ID, requests.First().ID);
            Assert.IsNotNull(requests.First().ID);
            Assert.IsFalse(requests[0].ID == requests[5].ID);
        }

        //[Test]
        //public void Test_GetRequestsByUser()
        //{
        //    var requests = this._leaveBookingRepository.GetRequestsByUser(1);

        //    foreach(var request in requests)
        //    {
        //        Assert.AreEqual(request.UserID, 1);
        //    }

        //    //Assert.IsTrue(requests.All(rq => rq.UserID == 1));

        //    Assert.AreEqual(2, requests.Count);
        //    Assert.IsTrue(requests.First().StatusID == 2);
        //    Assert.IsTrue(requests.Last().StatusID == 3);
        //    Assert.IsTrue(requests.First().StartDate == requests.Last().StartDate);

        //    var secondRequest = this._leaveBookingRepository.GetRequestsByUser(2);
        //    Assert.IsTrue(secondRequest.First().StatusID == 1);
        //}

        //[Test]
        //public void Test_GetRequestsByUserAndYear()
        //{
        //    var filteredRequests = this._leaveBookingRepository.GetRequestsByUserAndYear(1, 2018);
        //    Assert.AreEqual(2, filteredRequests.Count);
        //    Assert.IsTrue(filteredRequests.First().StartDate.Year == 2018);
        //    Assert.IsFalse(filteredRequests.First().StartDate.Year == 2017);
        //    Assert.IsTrue(filteredRequests.First().StartDate == filteredRequests.Last().StartDate);
        //}

        [Test]
        public void Test_GetRequestsByUserAndYearAndStatus()
        {
            var bookedRequests = this._leaveBookingRepository.GetRequestsByUserAndYearAndStatus(1, 2018, RequestStatus.Accepted);
            Assert.IsNotNull(bookedRequests);
            Assert.AreEqual(1, bookedRequests.Count);
            Assert.IsTrue(bookedRequests.First().StatusID == 2);
        }

        //[Test]
        //public void Test_GetTeamManagerByManagerID()
        //{
        //    var teamManager = this._leaveBookingRepository.GetTeamManagerByManagerID(6);
        //    Assert.AreEqual(1, teamManager.Count);
        //    Assert.IsTrue(teamManager.All(rq => rq.ID == 6));
        //    Assert.IsTrue(teamManager.First().TeamID == 1);
        //    Assert.IsTrue(teamManager.First().IsManager);
        //}

        [Test]
        public void Test_GetUsersByTeamID()
        {
            var users = this._leaveBookingRepository.GetUsersByTeamID(6);
            Assert.AreEqual(7, users.Count);
            Assert.IsTrue(users.All(rq => rq.TeamID == 1));
            Assert.IsTrue(users[5].IsManager);
        }

        //[Test]
        //public void Test_GetTeamUsersByUserID()
        //{
        //    var teamUsers = this._leaveBookingRepository.GetTeamUsersByUserID(
        //}

        [Test]
        public void Test_GetRequestsByUsersInTeam()
        {
            var totalRequests = this._leaveBookingRepository.GetRequestsByUsersInTeam(6);
            Assert.AreEqual(6, totalRequests.Count);
            Assert.IsTrue(totalRequests.First().StatusID == 2);
        }

        [Test]
        public void Test_GetPendingRequestsByManagerID()
        {
            var pendingRequests = this._leaveBookingRepository.GetPendingRequestsByManagerID(6, RequestStatus.Outstanding);
            Assert.AreEqual(2, pendingRequests.Count);
            Assert.IsTrue(pendingRequests.All(rq => rq.StatusID == 1));
            Assert.IsTrue(pendingRequests.First().UserID == 2);
            Assert.IsTrue(pendingRequests.Last().UserID == 5);
        }

        [Test]
        public void Test_CancelRequest()
        {
            var requests = this._leaveBookingRepository.GetRequests();
            var previousRequestsCount = requests.Count;
            Assert.IsTrue(requests[2].StatusID == 2);
            Assert.AreEqual(6, previousRequestsCount);

            this._leaveBookingRepository.CancelRequest(3);

            var updatedRequests = this._leaveBookingRepository.GetRequests();
            var latestRequestsCount = updatedRequests.Count;

            Assert.AreEqual(previousRequestsCount, latestRequestsCount);
            Assert.AreEqual(6, latestRequestsCount);
            //Assert.IsFalse(requests[2].StatusID == updatedRequests[2].StatusID);
            Assert.IsTrue(updatedRequests[2].StatusID == 4);
            Assert.IsTrue(updatedRequests[2].UserID == 3);
        }

        [Test]
        public void Test_ApproveOrRejectRequest()
        {
            var requests = this._leaveBookingRepository.GetRequests();
            var previousRequestsCount = requests.Count;
            Assert.AreEqual(6, previousRequestsCount);
            Assert.IsTrue(requests[1].StatusID == 1);

            this._leaveBookingRepository.ApproveOrRejectRequest(2, RequestStatus.Accepted);

            var updatedRequests = this._leaveBookingRepository.GetRequests();
            var latestRequestsCount = updatedRequests.Count;

            Assert.AreEqual(previousRequestsCount, latestRequestsCount);
            Assert.AreEqual(6, latestRequestsCount);
            //Assert.IsFalse(requests[1].StatusID == updatedRequests[1].StatusID);
            Assert.IsTrue(updatedRequests[1].StatusID == 2);
            Assert.IsTrue(updatedRequests[1].UserID == 2);

            this._leaveBookingRepository.ApproveOrRejectRequest(5, RequestStatus.Rejected);

            var moreUpdatedRequests = this._leaveBookingRepository.GetRequests();
            var finalRequestsCount = moreUpdatedRequests.Count;

            Assert.AreEqual(6, finalRequestsCount);
            Assert.IsTrue(moreUpdatedRequests[4].StatusID == 3);
            Assert.IsTrue(moreUpdatedRequests[4].UserID == 5);
        }

        [Test]
        public void Test_GetTeams()
        {
            var teams = this._leaveBookingRepository.GetTeams();

            Assert.AreEqual(3, teams.Count);
            Assert.IsTrue(teams.First().ID == 1);
            Assert.IsTrue(teams.Last().ID == 3);
            Assert.IsTrue(teams.First().TeamName == "Team1");
        }

        [Test]
        public void Test_GetAccessLevels()
        {
            var accessLevels = this._leaveBookingRepository.GetAccessLevels();

            Assert.AreEqual(3, accessLevels.Count);
            Assert.IsTrue(accessLevels.First().ID == 1);
            Assert.IsTrue(accessLevels.First().Level == "User");
            Assert.IsTrue(accessLevels[1].Level == "Manager");
        }

        [Test]
        public void Test_UpdateUserAccessLevel()
        {
            var users = this._leaveBookingRepository.GetUsers();
            Assert.IsTrue(users[2].AccessLevelID == 1);

            this._leaveBookingRepository.UpdateUserAccessLevel(3, 2);

            var updatedUsers = this._leaveBookingRepository.GetUsers();
            Assert.IsTrue(users[2].AccessLevelID == 2);
        }

        [Test]
        public void Test_UpdateUserAllowance()
        {
            var users = this._leaveBookingRepository.GetUsers();
            Assert.IsTrue(users[3].Allowance == 25);

            this._leaveBookingRepository.UpdateUserAllowance(4, 27);

            var updatedUsers = this._leaveBookingRepository.GetUsers();
            Assert.IsTrue(users[3].Allowance == 27);
        }
    }
}