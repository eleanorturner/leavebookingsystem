
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 07/09/2018 12:38:39
-- Generated from EDMX file: C:\Projects\LeaveBookingSystem\LeaveBookingSystem\Models\Model1.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [LeaveBookingSystem];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_Request_RequestStatus]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Requests] DROP CONSTRAINT [FK_Request_RequestStatus];
GO
IF OBJECT_ID(N'[dbo].[FK_Request_User]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Requests] DROP CONSTRAINT [FK_Request_User];
GO
IF OBJECT_ID(N'[dbo].[FK_Team_User]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Teams] DROP CONSTRAINT [FK_Team_User];
GO
IF OBJECT_ID(N'[dbo].[FK_TeamManager_Team]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[TeamManagers] DROP CONSTRAINT [FK_TeamManager_Team];
GO
IF OBJECT_ID(N'[dbo].[FK_TeamManager_User]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[TeamManagers] DROP CONSTRAINT [FK_TeamManager_User];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[Requests]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Requests];
GO
IF OBJECT_ID(N'[dbo].[RequestStatus]', 'U') IS NOT NULL
    DROP TABLE [dbo].[RequestStatus];
GO
IF OBJECT_ID(N'[dbo].[Teams]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Teams];
GO
IF OBJECT_ID(N'[dbo].[TeamManagers]', 'U') IS NOT NULL
    DROP TABLE [dbo].[TeamManagers];
GO
IF OBJECT_ID(N'[dbo].[Users]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Users];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'Requests'
CREATE TABLE [dbo].[Requests] (
    [ID] int  NOT NULL,
    [UserID] int  NOT NULL,
    [StartDate] datetime  NOT NULL,
    [EndDate] datetime  NOT NULL,
    [Number_of_days] int  NOT NULL,
    [UserNotes] nvarchar(max)  NULL,
    [Status] int  NOT NULL,
    [ManagerNotes] nvarchar(max)  NULL
);
GO

-- Creating table 'RequestStatus'
CREATE TABLE [dbo].[RequestStatus] (
    [ID] int IDENTITY(1,1) NOT NULL,
    [Status] nvarchar(50)  NOT NULL,
    [ManagerNotes] nvarchar(max)  NULL
);
GO

-- Creating table 'Teams'
CREATE TABLE [dbo].[Teams] (
    [ID] int IDENTITY(1,1) NOT NULL,
    [UserID] int  NOT NULL
);
GO

-- Creating table 'TeamManagers'
CREATE TABLE [dbo].[TeamManagers] (
    [ID] int IDENTITY(1,1) NOT NULL,
    [TeamID] int  NOT NULL,
    [UserID] int  NOT NULL
);
GO

-- Creating table 'Users'
CREATE TABLE [dbo].[Users] (
    [ID] int IDENTITY(1,1) NOT NULL,
    [FirstName] nvarchar(50)  NOT NULL,
    [LastName] nvarchar(50)  NOT NULL,
    [AccessLevelID] int  NOT NULL,
    [Allowance] int  NOT NULL,
    [Email] nvarchar(max)  NOT NULL,
    [IsActive] bit  NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [ID] in table 'Requests'
ALTER TABLE [dbo].[Requests]
ADD CONSTRAINT [PK_Requests]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'RequestStatus'
ALTER TABLE [dbo].[RequestStatus]
ADD CONSTRAINT [PK_RequestStatus]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'Teams'
ALTER TABLE [dbo].[Teams]
ADD CONSTRAINT [PK_Teams]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'TeamManagers'
ALTER TABLE [dbo].[TeamManagers]
ADD CONSTRAINT [PK_TeamManagers]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'Users'
ALTER TABLE [dbo].[Users]
ADD CONSTRAINT [PK_Users]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [Status] in table 'Requests'
ALTER TABLE [dbo].[Requests]
ADD CONSTRAINT [FK_Request_RequestStatus]
    FOREIGN KEY ([Status])
    REFERENCES [dbo].[RequestStatus]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Request_RequestStatus'
CREATE INDEX [IX_FK_Request_RequestStatus]
ON [dbo].[Requests]
    ([Status]);
GO

-- Creating foreign key on [UserID] in table 'Requests'
ALTER TABLE [dbo].[Requests]
ADD CONSTRAINT [FK_Request_User]
    FOREIGN KEY ([UserID])
    REFERENCES [dbo].[Users]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Request_User'
CREATE INDEX [IX_FK_Request_User]
ON [dbo].[Requests]
    ([UserID]);
GO

-- Creating foreign key on [UserID] in table 'Teams'
ALTER TABLE [dbo].[Teams]
ADD CONSTRAINT [FK_Team_User]
    FOREIGN KEY ([UserID])
    REFERENCES [dbo].[Users]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Team_User'
CREATE INDEX [IX_FK_Team_User]
ON [dbo].[Teams]
    ([UserID]);
GO

-- Creating foreign key on [TeamID] in table 'TeamManagers'
ALTER TABLE [dbo].[TeamManagers]
ADD CONSTRAINT [FK_TeamManager_Team]
    FOREIGN KEY ([TeamID])
    REFERENCES [dbo].[Teams]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_TeamManager_Team'
CREATE INDEX [IX_FK_TeamManager_Team]
ON [dbo].[TeamManagers]
    ([TeamID]);
GO

-- Creating foreign key on [UserID] in table 'TeamManagers'
ALTER TABLE [dbo].[TeamManagers]
ADD CONSTRAINT [FK_TeamManager_User]
    FOREIGN KEY ([UserID])
    REFERENCES [dbo].[Users]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_TeamManager_User'
CREATE INDEX [IX_FK_TeamManager_User]
ON [dbo].[TeamManagers]
    ([UserID]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------