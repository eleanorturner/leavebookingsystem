﻿using System;
using System.Configuration;
using System.Data.Entity;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Reflection;
using System.Data.Entity.Validation;
using System.Diagnostics;

using Empiricom.Framework.Common.Logging;
using Empiricom.Framework.DAL.EntityFramework;

namespace Ditto.AI.LeaveBookingSystem.DAL.Models
{
    partial class LeaveBookingSystemEntities : DbContext
    {
        //public LeaveBookingSystemEntities(string sConnectionString)
        //    : base(sConnectionString)
        //{
        //    //_contextInstance = this;
        //}


        [ThreadStatic]
        private static LeaveBookingSystemEntities _contextInstance;

        static LeaveBookingSystemEntities()
        {
        }


        public static void StartContext()
        {
            _contextInstance = null;
        }

        public static void RestartContext()
        {
            _contextInstance = null;
        }

        public static void EndContext()
        {
            _contextInstance = null;
        }

        public static LeaveBookingSystemEntities Instance
        {
            get
            {
                if (_contextInstance == null)
                {
                    _contextInstance = new LeaveBookingSystemEntities();
                }

                return _contextInstance;
            }
        }

        public void DeleteEntity(EntityBase entity)
        {
            entity.OnBeforeDelete();
            Entry(entity).State = EntityState.Deleted;
        }

        public void ReplaceEntity(EntityBase oldEntity, EntityBase newEntity)
        {
            if (oldEntity == null)
            {
                throw new ArgumentNullException("oldEntity");
            }

            if (newEntity == null)
            {
                throw new ArgumentNullException("newEntity");
            }

            Instance.Entry(oldEntity).State = EntityState.Detached;
            Instance.Entry(newEntity).State = EntityState.Modified;

            PropertyInfo[] properties = oldEntity.GetType().GetProperties();
            foreach (PropertyInfo property in
                properties.Where(
                    p =>
                    typeof(IEntityCollection).IsAssignableFrom(p.PropertyType)))
            {
                Instance.Entry(newEntity).Collection(property.Name).Load();
            }
        }

        public override int SaveChanges()
        {
            string userName = string.Empty;
            if (System.Threading.Thread.CurrentPrincipal.Identity != null && !string.IsNullOrEmpty(System.Threading.Thread.CurrentPrincipal.Identity.Name))
            {
                userName = System.Threading.Thread.CurrentPrincipal.Identity.Name;
            }

            // Save changes
            try
            {
                IList<EntityBase> savedEntities = new List<EntityBase>();
                foreach (var changedEntity in ChangeTracker.Entries().Where(e => e.State == EntityState.Added || e.State == EntityState.Modified))
                {
                    EntityBase entity = changedEntity.Entity as EntityBase;
                    if (entity != null)
                    {
                        entity.OnBeforeSave();
                        savedEntities.Add(entity);
                    }
                }

                int result = base.SaveChanges();
                foreach (EntityBase savedEntity in savedEntities)
                {
                    Entry(savedEntity).Reload();
                }
                return result;
            }
            catch (DbEntityValidationException e)
            {
                Logger logger = Logger.GetInstance(typeof(DbContext));
                foreach (var eve in e.EntityValidationErrors)
                {
                    string message = string.Format(
                            "Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                            eve.Entry.Entity.GetType().Name,
                            eve.Entry.State);

                    logger.Error(message);
                    Trace.WriteLine(message);

                    foreach (var ve in eve.ValidationErrors)
                    {
                        message = string.Format("- Property: \"{0}\", Error: \"{1}\"", ve.PropertyName, ve.ErrorMessage);
                        logger.Error(message);
                        Trace.WriteLine(message);
                    }
                }

                throw;
            }
            catch (Exception exception)
            {
                Logger logger = Logger.GetInstance(typeof(DbContext));
                string innerExceptionMessage = exception.Message;

                while (exception.InnerException != null)
                {
                    innerExceptionMessage += string.Format(" {0}", exception.InnerException.Message);
                    exception = exception.InnerException;
                }

                string errorMessage = string.Format("Error saving Entities - {0}", innerExceptionMessage).Trim();
                logger.Error(errorMessage);
                Trace.Write(errorMessage);

                throw;
            }
        }





    }
}





   
