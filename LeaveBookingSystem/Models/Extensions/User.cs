﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ditto.AI.LeaveBookingSystem.DAL.Models
{
    public partial class User
    {
        //string _extraInfo = "";

        public bool HasTeam {
            get
            {
                if(this.TeamUsers.Count == 0)
                {
                    return false;
                }

                bool userIsManager = this.TeamUsers.First().Team.TeamUsers.Any(tu => tu.UserID == this.ID && tu.IsManager);
                return userIsManager;
             }
        }


        //public string  ExtraInfo
        //{
        //    get { return _extraInfo; }

        //    set { _extraInfo = value; }
        //}
    }
}