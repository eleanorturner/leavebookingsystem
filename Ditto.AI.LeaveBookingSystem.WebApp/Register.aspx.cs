﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security;
using System;
using System.Linq;
using System.Web;
using Ditto.AI.LeaveBookingSystem.BLL;

namespace WebFormsIdentity
{
    public partial class Register : System.Web.UI.Page
    {
        Ditto.AI.LeaveBookingSystem.BLL.ILeaveBookingRepository bll = new LeaveBookingRepository();

        protected void Page_Load()
        {
            //
        }

        protected void CreateUser_Click(object sender, EventArgs e)
        {
            // Default UserStore constructor uses the default connection string named: DefaultConnection
            var userStore = new UserStore<IdentityUser>();
            var manager = new UserManager<IdentityUser>(userStore);
            var user = new IdentityUser() { UserName = UserName.Text };

            IdentityResult result = manager.Create(user, Password.Text);

            if (result.Succeeded)
            {
                //create a new user in Leavebookingsystem db
                //with first name, last name, team, email (username)

                //save to db
                //createUser
                string email = UserName.Text;
                string firstName = FirstName.Text;
                string lastName = LastName.Text;
                int team = Convert.ToInt32(DDL.SelectedValue);

                bll.CreateUser(email, firstName, lastName, team);

                var authenticationManager = HttpContext.Current.GetOwinContext().Authentication;
                var userIdentity = manager.CreateIdentity(user, DefaultAuthenticationTypes.ApplicationCookie);
                authenticationManager.SignIn(new AuthenticationProperties() { }, userIdentity);
                Response.Redirect("~/Login.aspx");
            }
            else
            {
                StatusMessage.Text = result.Errors.FirstOrDefault();
            }
        }
    }
}