﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Register.aspx.cs" Inherits="WebFormsIdentity.Register" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body style="font-family: Arial, Helvetica, sans-serif; font-size: small">
    <form id="form1" runat="server">
    <div id="header" style="width:100%;height:90px;font-family:'Nirmala UI';background-color:#eeeeee;color:#FF771D;font-size:40px;text-align:center;padding-top:30px;border-bottom:1px solid #FF771D;">
        Leave Booking System
    </div>
    <div id="body" style="text-align:center;padding-top:30px;font-family:'Nirmala UI'">
        <h4 style="font-size: large">Register a new user</h4>
        <p>
            <asp:Literal runat="server" ID="StatusMessage" />
        </p>
    </div>
    <table id="myLoginTable" style="border:none;margin:0px auto" width="500px" height="300px" align="centre">
        <tr style="margin-bottom:10px; margin-top:20px;padding-bottom:10px">
            <td style="text-align:right"><asp:Label runat="server" style="font-size:17px;font-family:'Nirmala UI'">First name</asp:Label></td>
            <td style="padding-left:20px"><asp:TextBox runat="server" ID="FirstName" style="margin-left:10px;font-family:'Nirmala UI'"/></td>            
        </tr>
        <tr style="margin-bottom:10px; margin-top:20px; font-size:17px;font-family:'Nirmala UI'">
            <td style="text-align:right"><asp:Label runat="server">Last name</asp:Label></td>
            <td style="padding-left:20px"><asp:TextBox runat="server" ID="LastName" style="margin-left:10px;font-family:'Nirmala UI'"/></td>
        </tr>
        <tr style="margin-bottom:10px; margin-top:20px; font-size:17px;font-family:'Nirmala UI'">
            <td style="text-align:right"><asp:Label runat="server">Team</asp:Label></td>
            <td style="padding-left:20px"><asp:DropDownList ID="DDL" runat="server" style="margin-left:10px;font-family:'Nirmala UI'" DataSourceID="ObjectDataSource1" DataTextField="TeamName" DataValueField="ID" >
                </asp:DropDownList>
                <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" SelectMethod="GetTeams" TypeName="Ditto.AI.LeaveBookingSystem.BLL.LeaveBookingRepository"></asp:ObjectDataSource></td>
        </tr>
        <tr style="margin-bottom:10px; margin-top:20px; font-size:17px;font-family:'Nirmala UI'">
            <td style="text-align:right"><asp:Label runat="server" AssociatedControlID="UserName">Email</asp:Label></td>
            <td style="padding-left:20px"><asp:TextBox runat="server" ID="UserName" style="margin-left:10px;font-family:'Nirmala UI'"/></td>
        </tr>
        <tr style="margin-bottom:10px; margin-top:20px; font-size:17px;font-family:'Nirmala UI'">
            <td style="text-align:right"><asp:Label runat="server" AssociatedControlID="Password">Password</asp:Label></td>
            <td style="padding-left:20px"><asp:TextBox runat="server" ID="Password" TextMode="Password" style="margin-left:10px;font-family:'Nirmala UI'"/></td>
        </tr>
        <tr style="margin-bottom:10px; margin-top:20px; font-size:17px;font-family:'Nirmala UI'">
            <td style="text-align:right"><asp:Label runat="server" AssociatedControlID="ConfirmPassword">Confirm password</asp:Label></td>
            <td style="padding-left:20px"><asp:TextBox runat="server" ID="ConfirmPassword" TextMode="Password" style="margin-left:10px;font-family:'Nirmala UI'"/></td>
        </tr>
    </table>
        <div style="margin-bottom:10px; margin-top:40px;font-family:'Nirmala UI';text-align:center">
            <a href="javascript:window.location.href='/Login.aspx'" style="color:#611dff;font-family:'Nirmala UI'">Already have an account?</a>
        </div>
        <div style="margin-top:40px;text-align:center">
            <asp:Button runat="server" OnClick="CreateUser_Click" Text="Register" style="border-color:#FF771D;background-color:#f2f2f2;font-family:'Nirmala UI';padding-bottom:4px"/>
        </div>
    </form>
</body>
</html>
