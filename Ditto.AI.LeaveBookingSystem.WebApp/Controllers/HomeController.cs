﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ditto.AI.LeaveBookingSystem.BLL;

namespace Ditto.AI.LeaveBookingSystem.WebApp.Controllers
{
    public class HomeController : Controller
    {
        Ditto.AI.LeaveBookingSystem.BLL.ILeaveBookingRepository bll = new LeaveBookingRepository();

        public ActionResult Index()
        {
            ViewBag.Message = "Index.";

            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }


        public ActionResult UserRequestForm()
        {
            ViewBag.Message = "Your request page.";

            //bll.CreateRequest(1,)

            return View();
        }


        public JsonResult SaveRequest(string startDate, string endDate, int duration, string userNotes)
        {
            DateTime newStartDate =  DateTime.Parse(startDate);
            DateTime newEndDate = DateTime.Parse(endDate);

            int userId = Convert.ToInt32(Session["UserID"]);
            bll.CreateRequest(userId, newStartDate, newEndDate, duration, userNotes);

            return Json(true, JsonRequestBehavior.AllowGet);

        }

        public ActionResult CancelRequest()
        {
            ViewBag.Message = "Your cancellation page.";

            var currentYear = DateTime.Now.Year;
            int userID = Convert.ToInt32(Session["UserID"]);

            IList<DAL.Models.Request> acceptedRequests = bll.GetRequestsByUserAndYearAndStatus(userID, currentYear, RequestStatus.Accepted);

            return View(acceptedRequests);
        }

        public JsonResult DeleteRequest(int requestID)
        {
            var requestId = requestID;
            bll.CancelRequest(requestId);

            return Json(true, JsonRequestBehavior.AllowGet);
        }

        public ActionResult BookedLeave()
        {
            ViewBag.Message = "Your booked leave page.";

            var currentYear = DateTime.Now.Year;
            int userID = Convert.ToInt32(Session["UserID"]);

            IList<DAL.Models.Request> requests = bll.GetRequestsByUserAndYearAndStatus(userID, currentYear, RequestStatus.Accepted);

            return View(requests);
        }

        public ActionResult Team()
        {
            ViewBag.Message = "Your team page.";

            int managerUserID = Convert.ToInt32(Session["UserID"]);

            IList<DAL.Models.TeamUser> users = bll.GetUsersByTeamID(managerUserID);

            return View(users);
        }

        public ActionResult TeamLeave()
        {
            ViewBag.Message = "Your team's booked leave page.";

            int managerUserID = Convert.ToInt32(Session["UserID"]);

            IList<DAL.Models.Request> teamRequests = bll.GetPendingRequestsByManagerID(managerUserID, RequestStatus.Accepted);

            return View(teamRequests);
        }

        public ActionResult PendingRequests()
        {
            ViewBag.Message = "Your team's pending leave page.";

            int managerUserID = Convert.ToInt32(Session["UserID"]);

            IList<DAL.Models.Request> pendingTeamRequests = bll.GetPendingRequestsByManagerID(managerUserID, RequestStatus.Outstanding);

            return View(pendingTeamRequests);
        }

        public JsonResult AcceptRequest(int requestID)
        {
            var requestId = requestID;
            bll.ApproveOrRejectRequest(requestId, RequestStatus.Accepted);

            return Json(true, JsonRequestBehavior.AllowGet);
        }

        public JsonResult RejectRequest(int requestID)
        {
            var requestId = requestID;
            bll.ApproveOrRejectRequest(requestId, RequestStatus.Rejected);

            return Json(true, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SubmittedRequests()
        {
            ViewBag.Message = "Your submitted requests page.";

            var currentYear = DateTime.Now.Year;
            int userID = Convert.ToInt32(Session["UserID"]);

            IList<DAL.Models.Request> submittedRequests = bll.GetRequestsByUserAndYearAndStatus(userID, currentYear, RequestStatus.Outstanding);

            return View(submittedRequests);
        }

        public JsonResult UpdateNotesOnRequest(int requestID, string notesText)
        {
            bll.UpdateNotesOnRequest(requestID, notesText);

            return Json(true, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Login()
        {
            ViewBag.Message = "Your login page.";

            return View();
        }

        public ActionResult AdminPage()
        {
            ViewBag.Message = "Your admin page.";

            IList<DAL.Models.User> users = bll.GetUsers();

            IList<DAL.Models.UserAccessLevel> userAccessLevels = bll.GetAccessLevels();
            ViewBag.UserAccessLevels = userAccessLevels;


            return View(users);
        }

        public JsonResult UpdateUserAccessLevel(int userID, int userAccessLevelID)
        {
            bll.UpdateUserAccessLevel(userID, userAccessLevelID);
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        public JsonResult UpdateUserAllowance(int userID, int userAllowance)
        {
            bll.UpdateUserAllowance(userID, userAllowance);
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Logout()
        {
            var AuthenticationManager = HttpContext.GetOwinContext().Authentication;
            AuthenticationManager.SignOut();
            return Json(true, JsonRequestBehavior.AllowGet);
        }
    }
}