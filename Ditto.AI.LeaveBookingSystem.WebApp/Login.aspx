﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="WebFormsIdentity.Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
   <title>Login</title>
</head>
<body style="font-family: Arial, Helvetica, sans-serif; font-size: small">
   <form id="form1" runat="server">
      <div id="header" style="width:100%;height:90px;font-family:'Nirmala UI';background-color:#eeeeee;color:#FF771D;font-size:40px;text-align:center;padding-top:30px;border-bottom:1px solid #FF771D;">
          Leave Booking System
      </div>
      <div id="body" style="text-align:center;padding-top:30px;font-family:'Nirmala UI'">
         <h4 style="font-size: large">Log In</h4>
         <asp:PlaceHolder runat="server" ID="LoginStatus" Visible="false">
            <p>
               <asp:Literal runat="server" ID="StatusText" />
            </p>
         </asp:PlaceHolder>
         <asp:PlaceHolder runat="server" ID="LoginForm" Visible="false">
            <div style="margin-bottom: 10px; font-size: 17px; margin-top: 40px;font-family:'Nirmala UI'">
               <asp:Label runat="server" AssociatedControlID="Email">Email</asp:Label>
               <div>
                  <asp:TextBox runat="server" ID="Email" style="font-family:'Nirmala UI'"/>
               </div>
            </div>
            <div style="margin-bottom: 10px; font-size: 17px; margin-top: 20px;font-family:'Nirmala UI'">
               <asp:Label runat="server" AssociatedControlID="Password">Password</asp:Label>
               <div>
                  <asp:TextBox runat="server" ID="Password" TextMode="Password" style="font-family:'Nirmala UI'"/>
               </div>
            </div>
            <div style="margin-bottom:10px; margin-top:40px">
                <a href="javascript:window.location.href='/Register.aspx'" style="color:#611dff;font-family:'Nirmala UI';">Don't have an account?</a>
            </div>
            <div style="margin-bottom: 10px">
               <div style="margin-top: 40px">
                  <asp:Button runat="server" OnClick="SignIn" Text="Log in" style="border-color:#FF771D;background-color:#f2f2f2;font-family:'Nirmala UI';padding-bottom:4px"/>
               </div>
            </div>
         </asp:PlaceHolder>
         <asp:PlaceHolder runat="server" ID="LogoutButton" Visible="false">
            <div>
               <div>
                  <asp:Button runat="server" OnClick="SignOut" Text="Log out" />
               </div>
            </div>
         </asp:PlaceHolder>
      </div>
   </form>
</body>
</html>