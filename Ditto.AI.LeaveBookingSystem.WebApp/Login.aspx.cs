﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security;
using System;
using System.Web;
using System.Web.UI.WebControls;
using Ditto.AI.LeaveBookingSystem.BLL;
using System.Collections.Generic;

namespace WebFormsIdentity
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Ditto.AI.LeaveBookingSystem.BLL.ILeaveBookingRepository bll = new LeaveBookingRepository();

            if (!IsPostBack)
            {
                if (User.Identity.IsAuthenticated)
                {
                    StatusText.Text = string.Format("Hello {0}!!", User.Identity.GetUserName());
                    LoginStatus.Visible = true;
                    LogoutButton.Visible = true;

                    string userEmail = User.Identity.GetUserName();

                    IList<Ditto.AI.LeaveBookingSystem.DAL.Models.User> users = bll.GetUsersByEmail(userEmail);
                    if(users.Count>0)
                    {
                        var userId = users[0].ID;
                        Session["UserID"] = userId;

                        var userAccessLevel = users[0].AccessLevelID;
                        Session["UserAccessLevel"] = userAccessLevel;

                        var userHasTeam = users[0].HasTeam;
                        Session["UserHasTeam"] = userHasTeam;


                        //var userID = Convert.ToInt32(Session["UserID"]);

                        Response.Redirect("~/Home/BookedLeave");
                    }
                    else
                    {
                        //show error message
                    }
                   

                    
                }
                else
                {
                    LoginForm.Visible = true;
                }
            }
        }

        protected void SignIn(object sender, EventArgs e)
        {
            var userStore = new UserStore<IdentityUser>();
            var userManager = new UserManager<IdentityUser>(userStore);
            var user = userManager.Find(Email.Text, Password.Text);

            if (user != null)
            {
                var authenticationManager = HttpContext.Current.GetOwinContext().Authentication;
                var userIdentity = userManager.CreateIdentity(user, DefaultAuthenticationTypes.ApplicationCookie);

                authenticationManager.SignIn(new AuthenticationProperties() { IsPersistent = false }, userIdentity);
                Response.Redirect("~/Login.aspx");
            }
            else
            {
                StatusText.Text = "Invalid username or password.";
                LoginStatus.Visible = true;
            }
        }

        protected void SignOut(object sender, EventArgs e)
        {
            var authenticationManager = HttpContext.Current.GetOwinContext().Authentication;
            authenticationManager.SignOut();
            Response.Redirect("~/Login.aspx");
        }
    }
}