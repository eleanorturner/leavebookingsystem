﻿using System.Web;
using System.Web.Mvc;

namespace Ditto.AI.LeaveBookingSystem.WebApp
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
