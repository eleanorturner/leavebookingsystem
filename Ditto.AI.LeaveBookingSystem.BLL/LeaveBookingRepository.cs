﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ditto.AI.LeaveBookingSystem.DAL;

namespace Ditto.AI.LeaveBookingSystem.BLL
{

    public class LeaveBookingRepository : ILeaveBookingRepository
    {
        DAL.Models.LeaveBookingSystemEntities _context = new DAL.Models.LeaveBookingSystemEntities();

        public int TestMethod()
        {
            return 7;
        }

        public void CreateUser(string email, string firstName, string lastName, int teamID)
        {
            try
            {
                DAL.Models.User newUser = new DAL.Models.User();

                newUser.FirstName = firstName;
                newUser.LastName = lastName;
                newUser.AccessLevelID = 1;
                newUser.Allowance = 25;
                newUser.Email = email;
                newUser.IsActive = true;

                DAL.Models.TeamUser teamUser = new DAL.Models.TeamUser();
                teamUser.TeamID = teamID;
                teamUser.User = newUser;

                newUser.TeamUsers.Add(teamUser);

                _context.Users.Add(newUser);
                _context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IList<DAL.Models.User> GetUsers()
        {
            try
            {
                return _context.Users.ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IList<DAL.Models.User> GetUsersByEmail(string email)
        {
            try
            {
                IList<DAL.Models.User> users = _context.Users.Where(rq => rq.Email == email).ToList();
                return users;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void CreateRequest(int userId, DateTime startDate, DateTime endDate, int numOfDays, string userNotes)
        {
            try
            {
                DAL.Models.Request newRequest = new DAL.Models.Request();

                newRequest.UserID = userId;
                newRequest.StartDate = startDate;
                newRequest.EndDate = endDate;
                newRequest.NumOfDays = numOfDays;
                newRequest.UserNotes = userNotes;
                newRequest.StatusID = 1;

                _context.Requests.Add(newRequest);
                _context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public IList<DAL.Models.Request> GetRequests()
        {
            try
            {
                return _context.Requests.ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //public IList<DAL.Models.Request> GetRequestsByUser(int userId)
        //{
        //    try
        //    {
        //        IList<DAL.Models.Request> requests = _context.Requests.Where(rq => rq.UserID == userId).ToList();
        //        return requests;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        //public IList<DAL.Models.Request> GetRequestsByUserAndYear(int userId, int year)
        //{
        //    try
        //    {
        //        var requests = GetRequestsByUser(userId);
        //        IList<DAL.Models.Request> filteredRequests = requests.Where(rq => rq.StartDate.Year == year).ToList();
        //        return filteredRequests;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }

        //}

        public IList<DAL.Models.Request> GetRequestsByUserAndYearAndStatus(int userId, int year, RequestStatus requestStatus)
        {
            try
            {
                IList<DAL.Models.Request> requests = _context.Requests.Where(rq => rq.UserID == userId).ToList();
                IList<DAL.Models.Request> filteredRequests = requests.Where(rq => rq.StartDate.Year == year).ToList();
                IList<DAL.Models.Request> bookedRequests = filteredRequests.Where(rq => rq.RequestStatu.Status == requestStatus.ToString()).ToList();
                return bookedRequests;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //public IList<DAL.Models.TeamUser> GetTeamManagerByManagerID(int managerUserId)
        //{
        //    try
        //    {
        //        //get TeamManager of the team that has 'manageruserid' as the manager
        //        IList<DAL.Models.TeamUser> teamManager = _context.TeamUsers.Where(rq => rq.UserID == managerUserId).ToList();

        //        return teamManager;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        public IList<DAL.Models.TeamUser> GetUsersByTeamID(int managerUserId)
        {
            try
            {
                //get TeamManager of the team that has 'manageruserid' as the manager
                IList<DAL.Models.TeamUser> teamManager = _context.TeamUsers.Where(rq => rq.UserID == managerUserId).ToList();

                //find TeamID of the manager's team
                int teamId = teamManager.First().TeamID;

                //get users in the team with the TeamID of the manager
                IList<DAL.Models.TeamUser> users = _context.TeamUsers.Where(rq => rq.TeamID == teamId).ToList();
                return users;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //public IList<DAL.Models.TeamUser> GetTeamUsersByUserID(int userId)
        //{
        //    try
        //    {
        //        IList<DAL.Models.TeamUser> user = _context.TeamUsers.Where(rq => rq.UserID == userId).ToList();
        //        int teamId = user.First().TeamID;

        //        IList<DAL.Models.TeamUser> teamUsers = _context.TeamUsers.Where(rq => rq.TeamID == teamId).ToList();
        //        return teamUsers;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        public IList<DAL.Models.Request> GetRequestsByUsersInTeam(int managerUserId)
        {
            try
            {
                //call GetUsersByTeamID
                var users = GetUsersByTeamID(managerUserId);
                List<DAL.Models.Request> totalRequests = new List<DAL.Models.Request>();

                //get requests for each user in the team
                foreach (var user in users)
                {
                    int userId = user.UserID;
                    IList<DAL.Models.Request> requests = _context.Requests.Where(rq => rq.UserID == userId).ToList();

                    totalRequests.AddRange(requests);
                }

                return totalRequests;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //public IList<DAL.Models.Request> GetPendingRequestsByManagerID(int managerUserId)
        //{
        //    try
        //    {
        //        var totalRequests = GetRequestsByUsersInTeam(managerUserId);

        //        List<DAL.Models.Request> pendingRequests = _context.Requests.Where(rq => rq.StatusID == 1).ToList();
        //        //method for 'As a manager I want a list so that I can view all pending annual leave requests for my team'
        //        //another IList which takes in status id as pending from enum as a parameter
        //        //probably just call the previous method 'GetRequestsByUsersInTeam' but with the added parameter

        //        return pendingRequests;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        public IList<DAL.Models.Request> GetPendingRequestsByManagerID(int managerUserId, RequestStatus requestStatus)
        {
            //'As a manager I want a list so that I can view all pending annual leave requests for my team
            try
            {
                var totalRequests = GetRequestsByUsersInTeam(managerUserId);
                List<DAL.Models.Request> pendingRequests = totalRequests.Where(rq => rq.RequestStatu.Status == requestStatus.ToString()).ToList();
                return pendingRequests;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public void CancelRequest(int requestId)
        {
            try
            {
                List<DAL.Models.Request> requestToCancel = _context.Requests.Where(rq => rq.ID == requestId).ToList();
                int cancelledValue = (int)RequestStatus.Cancelled;

                requestToCancel.First().StatusID = cancelledValue;
                _context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void ApproveOrRejectRequest(int requestId, RequestStatus requestStatus)
        {
            try
            {
                List<DAL.Models.Request> requestToChange = _context.Requests.Where(rq => rq.ID == requestId).ToList();
                int statusValue = (int)((RequestStatus)Enum.Parse(typeof(RequestStatus), requestStatus.ToString()));

                requestToChange.First().StatusID = statusValue;
                _context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }


        public void UpdateNotesOnRequest(int requestId, string notesText)
        {
            try
            {
                List<DAL.Models.Request> requestToChange = _context.Requests.Where(rq => rq.ID == requestId).ToList();
                
                requestToChange.First().ManagerNotes = notesText;
                _context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public IList<DAL.Models.Team> GetTeams()
        {
            try
            {
                return _context.Teams.ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IList<DAL.Models.UserAccessLevel> GetAccessLevels()
        {
            try
            {
                return _context.UserAccessLevels.ToList();
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateUserAccessLevel(int userId, int userAccessLevelId)
        {
            try
            {
                List<DAL.Models.User> userToChange = _context.Users.Where(rq => rq.ID == userId).ToList();

                userToChange.First().AccessLevelID = userAccessLevelId;
                _context.SaveChanges();
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateUserAllowance(int userId, int userAllowance)
        {
            try
            {
                List<DAL.Models.User> userToChange = _context.Users.Where(rq => rq.ID == userId).ToList();

                userToChange.First().Allowance = userAllowance;
                _context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}