﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ditto.AI.LeaveBookingSystem.BLL
{
    public interface ILeaveBookingRepository
    {
        int TestMethod();
        void CreateUser(string email, string firstName, string lastName, int teamID);
        IList<DAL.Models.User> GetUsers();
        IList<DAL.Models.User> GetUsersByEmail(string email);
        void CreateRequest(int userId, DateTime startDate, DateTime endDate, int numOfDays, string userNotes);
        IList<DAL.Models.Request> GetRequests();
        //IList<DAL.Models.Request> GetRequestsByUser(int userId);
        //IList<DAL.Models.Request> GetRequestsByUserAndYear(int userId, int year);
        IList<DAL.Models.Request> GetRequestsByUserAndYearAndStatus(int userId, int year, RequestStatus requestStatus);
        //IList<DAL.Models.TeamUser> GetTeamManagerByManagerID(int managerUserId);
        IList<DAL.Models.TeamUser> GetUsersByTeamID(int managerUserId);
        //IList<DAL.Models.TeamUser> GetTeamUsersByUserID(int userId);
        IList<DAL.Models.Request> GetRequestsByUsersInTeam(int managerUserId);
        IList<DAL.Models.Request> GetPendingRequestsByManagerID(int managerUserId, RequestStatus requestStatus);
        void CancelRequest(int requestId);
        void ApproveOrRejectRequest(int requestId, RequestStatus requestStatus);
        void UpdateNotesOnRequest(int requestId, string notesText);
        IList<DAL.Models.Team> GetTeams();
        IList<DAL.Models.UserAccessLevel> GetAccessLevels();
        void UpdateUserAccessLevel(int userId, int userAccessLevelId);
        void UpdateUserAllowance(int userId, int userAllowance);
    }
}
