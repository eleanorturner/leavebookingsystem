﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ditto.AI.LeaveBookingSystem.BLL
{
    public enum RequestStatus
    {
        Outstanding = 1,
        Accepted,
        Rejected,
        Cancelled
    }
}