﻿/****** Object:  Table [dbo].[AuditLog]    Script Date: 26/02/2016 15:19:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AuditLog](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[UserName] [nvarchar](250) NOT NULL,
	[EventDateUTC] [datetime2](7) NOT NULL,
	[TableName] [nvarchar](100) NOT NULL,
	[RecordID] [int] NULL,
	[EventType] [nvarchar](10) NOT NULL,
 CONSTRAINT [PK_AuditLog] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AuditLogDetail]    Script Date: 26/02/2016 15:19:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AuditLogDetail](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[PropertyName] [nvarchar](100) NOT NULL,
	[OriginalValue] [text] NULL,
	[NewValue] [text] NULL,
	[AuditLogID] [int] NOT NULL,
 CONSTRAINT [PK_AuditLogDetail] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
ALTER TABLE [dbo].[AuditLogDetail]  WITH CHECK ADD  CONSTRAINT [FK_AuditLogDetail_AuditLog] FOREIGN KEY([AuditLogID])
REFERENCES [dbo].[AuditLog] ([ID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AuditLogDetail] CHECK CONSTRAINT [FK_AuditLogDetail_AuditLog]
GO
